package sh.sb.stack;

/**
 * Created by administrator on 17-09-14.
 */
public class Stack<T> {

    Object[] lista= new Object[0];

    public void push(T i) {
        Object[] aux = new Object[lista.length + 1];

        for (int j = 0; j <lista.length ; j++) {
            aux[j] = lista[j];
        }

        aux[lista.length] = i;

        lista = aux;
    }

    public int size() {
        return lista.length;
    }

    public int pop() {
        int result= (int)lista[lista.length-1];

        Object[] aux =   new Object[lista.length-1];
        for (int j = 0; j <lista.length-1 ; j++) {
            aux[j] = lista[j];
        }

        lista = aux;

        return result;
    }
}
