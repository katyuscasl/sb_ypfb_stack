package sh.sb.stack;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by administrator on 17-09-14.
 */
public class StackTest {

    @Test
    public void testPush(){
        Stack<Integer> stack = new Stack<Integer>();
        stack.push(2);
        stack.push(4);
        int pop = stack.pop();
        assertTrue(stack.size() == 1);
        assertTrue(pop == 4);
    }
}
